<?php


use PHPUnit\Framework\TestCase;

include_once( (__DIR__) . '../../classes/Student.class.php');

class StudentTest extends TestCase{


	public function testName(){
		$nome = new Student();
		$nome->setName("Alysson");
		$retorno = $nome->getName();
		$this->assertEquals("Alysson", $retorno);	}



	public function testCreation(){
		$studentObj = new Student();
		$studentObj->setName("Alysson");
		$studentObj->setEmail("alysson@teste.com");
		$return = $studentObj->create();
		$this->assertEquals(true,$return);
	}
}

?>
